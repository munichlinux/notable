import React, { Component } from "react";
import axios from "axios";
import "./App.css";

function SideNavItem(props) {
  const { id, firstName, lastName } = props;
  const href = `/doctor/${id}/appointment`;
  return (
    <li>
      <a href={href}>
        {firstName},{lastName}
      </a>
    </li>
  );
}

function SideNavList(props) {
  const { doctors } = props;
  console.log(doctors);
  return (
    <ul>
      {doctors.map(doctor => (
        <SideNavItem key={doctor.id} {...doctor} />
      ))}
    </ul>
  );
}

function Appointment(props) {
  const { id, doctor_id, date, time } = props;

  return <li></li>;
}

function Appointments(props) {
  const { appointments } = props;

  return (
    <ul>
      appointments.map(item => (<Appointment key={item.id} {...item} />
      ))
    </ul>
  );
}

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { doctors: [] };
  }
  componentDidMount() {
    axios
      .get("/api")
      .then(resp => {
        this.setState({ doctors: resp.data });
      })
      .catch(e => {});
  }
  render() {
    const { doctors } = this.state;

    return <SideNavList doctors={doctors} />;
  }
}

export default App;
