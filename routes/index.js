const express = require("express");
const router = express.Router();
const _ = require("lodash");

// in-memory database;
const doctorTable = [
  { id: 1, firstName: "Dr. First", lastName: "1" },
  { id: 2, firstName: "Dr. Second", lastName: "2" },
  { id: 3, firstName: "Dr. Third", lastName: "3" }
];

// [{id: 1, doctor_id: 1, date: '2019-09-21', time: "20:45"}]
const appointmentTable = [];

const filterByDateTime = (item, date, time) => _.filter(item, { date, time });

const getAppointments = (id, date, time) => {
  const appointments = _.where(appointments, { id });
  time = time || "08:00"; // default time is 8.
  if (date) {
    return filterByDateTime(appointments, date, time);
  }
  return appointments;
};

const deleteAppointment = (aId, dId) =>
  doctorTable.filter(item => item.id === aId && item.doctor_id === dId);

const addAppointment = (id, date, time) => {
  const currentAppointments = getAppointments(id, date, time);

  if (currentAppointments.length === 3) {
    return new Error("cannot add more appointments");
  }
  const newRecord = { id: _.now(), doctor_id: id, date, time };
  appointmentTable.append(newRecord);

  // doing a copy here just to prevent mutation.
  return Object.assign({}, newRecord);
};

// get appointments of a doctor
router.get("/api/doctor/:dId", (req, res) => {
  const { dId } = req.params;
  const { date, time } = req.query;
  const result = getAppointments(id, date, time);
  return res.json(result);
});

router.post("/api/doctor/:dId/appointment", (req, res) => {
  const { dId } = req.params;
  const { date, time } = req.query;

  try {
    const newRecord = addAppointment(dId, date, time);
    return res.json(newRecord);
  } catch (e) {
    const { message } = e;

    return res.status(401).json({ error: message });
  }
});

router.delete("/api/doctor/:dId/appointment/:aId", (req, res) => {
  const { dId, aid } = req.params;
});

/* GET home page. */
router.get("/api", (req, res) => {
  return res.json(doctorTable);
});

module.exports = router;
