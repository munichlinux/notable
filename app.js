var express = require("express");
var path = require("path");
var favicon = require("serve-favicon");
var logger = require("morgan");
var cookieParser = require("cookie-parser");
var bodyParser = require("body-parser");

var index = require("./routes/index");

const hbs = require("hbs");
const Handlebars = require("handlebars");
const layouts = require("handlebars-layouts");
const helpers = require("handlebars-helpers")();
const fs = require("fs");
const _ = require("lodash");

Handlebars.registerHelper(layouts(Handlebars));
_.each(helpers, (fn, name) =>
  name ? Handlebars.registerHelper(name, fn) : false
);

const hbsInst = hbs.create(Handlebars);

var app = express();

const views = path.join(__dirname, "views");

Handlebars.registerPartial(
  "layout",
  fs.readFileSync(path.join(views, "layout.hbs"), "utf8")
);

// view engine setup
app.set("views", views);
app.engine("hbs", hbsInst.__express);
app.set("view engine", "hbs");

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/", index);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error("Not Found");
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
